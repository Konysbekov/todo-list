package com.example.todolist

import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import android.widget.RadioButton
import android.widget.RadioGroup

class SettingsActivity : AppCompatActivity() {

    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        val completedColorGroup: RadioGroup = findViewById(R.id.completed_color_group)
        val notCompletedColorGroup: RadioGroup = findViewById(R.id.not_completed_color_group)

        val savedCompletedColor = sharedPreferences.getInt("completed_color", R.color.green)
        val savedNotCompletedColor = sharedPreferences.getInt("not_completed_color", R.color.brown)

        selectRadioButton(completedColorGroup, savedCompletedColor)
        selectRadioButton(notCompletedColorGroup, savedNotCompletedColor)

        completedColorGroup.setOnCheckedChangeListener { _, checkedId ->
            val color = getColorFromRadioButtonId(checkedId)
            sharedPreferences.edit().putInt("completed_color", color).apply()
        }

        notCompletedColorGroup.setOnCheckedChangeListener { _, checkedId ->
            val color = getColorFromRadioButtonId(checkedId)
            sharedPreferences.edit().putInt("not_completed_color", color).apply()
        }
    }

    private fun selectRadioButton(radioGroup: RadioGroup, color: Int) {
        for (i in 0 until radioGroup.childCount) {
            val radioButton = radioGroup.getChildAt(i) as RadioButton
            if (getColorFromRadioButtonId(radioButton.id) == color) {
                radioButton.isChecked = true
                break
            }
        }
    }

    private fun getColorFromRadioButtonId(id: Int): Int {
        return when (id) {
            R.id.color_green -> R.color.green
            R.id.color_blue -> R.color.blue
            R.id.color_yellow -> R.color.yellow
            R.id.color_brown -> R.color.brown
            R.id.color_pink -> R.color.pink
            R.id.color_violet -> R.color.violet
            else -> R.color.green
        }
    }
}
