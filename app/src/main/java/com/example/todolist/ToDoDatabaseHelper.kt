package com.example.todolist

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class ToDoDatabaseHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_NAME = "todolist.db"
        private const val DATABASE_VERSION = 1

        const val TABLE_LISTS = "lists"
        const val TABLE_ITEMS = "items"

        const val COLUMN_LIST_ID = "id"
        const val COLUMN_LIST_NAME = "name"

        const val COLUMN_ITEM_ID = "id"
        const val COLUMN_ITEM_NAME = "name"
        const val COLUMN_ITEM_COMPLETED = "completed"
        const val COLUMN_ITEM_LIST_ID = "list_id"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val createListsTable = "CREATE TABLE $TABLE_LISTS (" +
                "$COLUMN_LIST_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$COLUMN_LIST_NAME TEXT)"
        val createItemsTable = "CREATE TABLE $TABLE_ITEMS (" +
                "$COLUMN_ITEM_ID INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "$COLUMN_ITEM_NAME TEXT, " +
                "$COLUMN_ITEM_COMPLETED INTEGER, " +
                "$COLUMN_ITEM_LIST_ID INTEGER, " +
                "FOREIGN KEY($COLUMN_ITEM_LIST_ID) REFERENCES $TABLE_LISTS($COLUMN_LIST_ID))"
        db?.execSQL(createListsTable)
        db?.execSQL(createItemsTable)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_ITEMS")
        db?.execSQL("DROP TABLE IF EXISTS $TABLE_LISTS")
        onCreate(db)
    }
}
