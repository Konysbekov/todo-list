package com.example.todolist

data class ToDoItem(
    val id: Long,
    val name: String,
    var isCompleted: Boolean,
    val listId: Long
)
