package com.example.todolist

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class NewListActivity : AppCompatActivity() {

    private lateinit var listNameEditText: EditText
    private lateinit var saveButton: Button
    private lateinit var repository: ToDoRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_list)

        listNameEditText = findViewById(R.id.list_name)
        saveButton = findViewById(R.id.save_button)
        repository = ToDoRepository(this)

        saveButton.setOnClickListener {
            val listName = listNameEditText.text.toString()
            if (listName.isNotEmpty()) {
                repository.addList(listName)
                finish()
            }
        }
    }
}
