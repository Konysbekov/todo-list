package com.example.todolist

import android.view.LayoutInflater
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.PopupMenu
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class ToDoListAdapter(
    private val lists: List<ToDoList>,
    private val onListClick: (ToDoList) -> Unit,
    private val onEditClick: (ToDoList) -> Unit,
    private val onDeleteClick: (ToDoList) -> Unit
) : RecyclerView.Adapter<ToDoListAdapter.ToDoListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToDoListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_todo_list, parent, false)
        return ToDoListViewHolder(view)
    }

    override fun onBindViewHolder(holder: ToDoListViewHolder, position: Int) {
        val todoList = lists[position]
        holder.bind(todoList, onListClick, onEditClick, onDeleteClick)
    }

    override fun getItemCount(): Int = lists.size

    class ToDoListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val listName: TextView = itemView.findViewById(R.id.list_name)
        private val menuButton: ImageButton = itemView.findViewById(R.id.menu_button)

        fun bind(
            todoList: ToDoList,
            onListClick: (ToDoList) -> Unit,
            onEditClick: (ToDoList) -> Unit,
            onDeleteClick: (ToDoList) -> Unit
        ) {
            listName.text = todoList.name
            itemView.setOnClickListener { onListClick(todoList) }
            menuButton.setOnClickListener {
                showPopupMenu(it, todoList, onEditClick, onDeleteClick)
            }
        }

        private fun showPopupMenu(view: View, todoList: ToDoList, onEditClick: (ToDoList) -> Unit, onDeleteClick: (ToDoList) -> Unit) {
            val popupMenu = PopupMenu(view.context, view)
            val inflater: MenuInflater = popupMenu.menuInflater
            inflater.inflate(R.menu.menu_todo_list, popupMenu.menu)
            popupMenu.setOnMenuItemClickListener { menuItem ->
                when (menuItem.itemId) {
                    R.id.edit -> {
                        onEditClick(todoList)
                        true
                    }
                    R.id.delete -> {
                        onDeleteClick(todoList)
                        true
                    }
                    else -> false
                }
            }
            popupMenu.show()
        }
    }
}
