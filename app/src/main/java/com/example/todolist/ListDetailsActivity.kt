package com.example.todolist

import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class ListDetailsActivity : AppCompatActivity() {

    private lateinit var listDetailsRecyclerView: RecyclerView
    private lateinit var addButton: FloatingActionButton
    private lateinit var adapter: ToDoItemAdapter
    private lateinit var repository: ToDoRepository
    private var listId: Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_details)

        listDetailsRecyclerView = findViewById(R.id.list_details_recycler_view)
        addButton = findViewById(R.id.add_button)
        repository = ToDoRepository(this)

        listDetailsRecyclerView.layoutManager = LinearLayoutManager(this)

        listId = intent.getLongExtra("LIST_ID", -1)
        fetchItems()

        addButton.setOnClickListener {
            displayAddItemDialog()
        }
    }

    private fun fetchItems() {
        val items = repository.getItems(listId)
        adapter = ToDoItemAdapter(items.toMutableList(),
            { item, isChecked ->
                item.isCompleted = isChecked
                repository.updateItem(item)
                adapter.updateItems(repository.getItems(listId))
            },
            { item -> displayEditItemDialog(item) },
            { item -> removeItem(item) }
        )
        listDetailsRecyclerView.adapter = adapter
    }

    private fun displayAddItemDialog() {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_item, null)
        val dialogBuilder = AlertDialog.Builder(this)
            .setView(dialogView)
            .setTitle("Add ToDo item")
            .setNegativeButton("CANCEL", null)
            .setPositiveButton("ADD") { _, _ ->
                val editText = dialogView.findViewById<EditText>(R.id.edit_text_item_name)
                val itemName = editText.text.toString()
                if (itemName.isNotEmpty()) {
                    repository.addItem(listId, itemName, false)
                    fetchItems()
                } else {
                    Toast.makeText(this, "Item name cannot be empty", Toast.LENGTH_SHORT).show()
                }
            }
        dialogBuilder.create().show()
    }

    private fun displayEditItemDialog(item: ToDoItem) {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_item, null)
        val editText = dialogView.findViewById<EditText>(R.id.edit_text_item_name)
        editText.setText(item.name)
        val dialogBuilder = AlertDialog.Builder(this)
            .setView(dialogView)
            .setTitle("Edit ToDo item")
            .setNegativeButton("CANCEL", null)
            .setPositiveButton("SAVE") { _, _ ->
                val itemName = editText.text.toString()
                if (itemName.isNotEmpty()) {
                    val updatedItem = item.copy(name = itemName)
                    repository.updateItem(updatedItem)
                    fetchItems()
                } else {
                    Toast.makeText(this, "Item name cannot be empty", Toast.LENGTH_SHORT).show()
                }
            }
        dialogBuilder.create().show()
    }

    private fun removeItem(item: ToDoItem) {
        repository.deleteItem(item.id)
        fetchItems()
    }

    override fun onResume() {
        super.onResume()
        fetchItems()
    }
}
