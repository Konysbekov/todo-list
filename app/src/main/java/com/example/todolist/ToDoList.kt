package com.example.todolist

data class ToDoList(
    val id: Long,
    val name: String,
    val items: List<ToDoItem> = emptyList()
)
