package com.example.todolist

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.button.MaterialButton

class MainActivity : AppCompatActivity() {

    private lateinit var todoListRecyclerView: RecyclerView
    private lateinit var addButton: FloatingActionButton
    private lateinit var settingsButton: MaterialButton
    private lateinit var adapter: ToDoListAdapter
    private lateinit var repository: ToDoRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        todoListRecyclerView = findViewById(R.id.todo_list_recycler_view)
        addButton = findViewById(R.id.add_button)
        settingsButton = findViewById(R.id.settings_button)
        repository = ToDoRepository(this)

        todoListRecyclerView.layoutManager = LinearLayoutManager(this)

        fetchLists()

        addButton.setOnClickListener {
            displayAddListDialog()
        }

        settingsButton.setOnClickListener {
            val intent = Intent(this, SettingsActivity::class.java)
            startActivity(intent)
        }
    }

    private fun fetchLists() {
        val lists = repository.getLists()
        adapter = ToDoListAdapter(lists,
            { list ->
                val intent = Intent(this, ListDetailsActivity::class.java)
                intent.putExtra("LIST_ID", list.id)
                startActivity(intent)
            },
            { list -> displayEditListDialog(list) },
            { list -> removeList(list) }
        )
        todoListRecyclerView.adapter = adapter
    }

    private fun displayAddListDialog() {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_list, null)
        val dialogBuilder = AlertDialog.Builder(this)
            .setView(dialogView)
            .setTitle("Add ToDo list")
            .setNegativeButton("CANCEL", null)
            .setPositiveButton("ADD") { _, _ ->
                val editText = dialogView.findViewById<EditText>(R.id.edit_text_list_name)
                val listName = editText.text.toString()
                if (listName.isNotEmpty()) {
                    repository.addList(listName)
                    fetchLists()
                } else {
                    Toast.makeText(this, "List name cannot be empty", Toast.LENGTH_SHORT).show()
                }
            }
        dialogBuilder.create().show()
    }

    private fun displayEditListDialog(list: ToDoList) {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_add_list, null)
        val editText = dialogView.findViewById<EditText>(R.id.edit_text_list_name)
        editText.setText(list.name)
        val dialogBuilder = AlertDialog.Builder(this)
            .setView(dialogView)
            .setTitle("Edit ToDo list")
            .setNegativeButton("CANCEL", null)
            .setPositiveButton("SAVE") { _, _ ->
                val listName = editText.text.toString()
                if (listName.isNotEmpty()) {
                    val updatedList = list.copy(name = listName)
                    repository.updateList(updatedList)
                    fetchLists()
                } else {
                    Toast.makeText(this, "List name cannot be empty", Toast.LENGTH_SHORT).show()
                }
            }
        dialogBuilder.create().show()
    }

    private fun removeList(list: ToDoList) {
        AlertDialog.Builder(this)
            .setTitle("Are you sure?")
            .setMessage("Do you want to delete this list?")
            .setNegativeButton("CANCEL", null)
            .setPositiveButton("CONTINUE") { _, _ ->
                repository.deleteList(list.id)
                fetchLists()
            }
            .create()
            .show()
    }

    override fun onResume() {
        super.onResume()
        fetchLists()
    }
}
