package com.example.todolist

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageButton
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView

class ToDoItemAdapter(
    private var items: MutableList<ToDoItem>,
    private val onItemCheckChange: (ToDoItem, Boolean) -> Unit,
    private val onEditClick: (ToDoItem) -> Unit,
    private val onDeleteClick: (ToDoItem) -> Unit
) : RecyclerView.Adapter<ToDoItemAdapter.ToDoItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToDoItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_todo_item, parent, false)
        return ToDoItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: ToDoItemViewHolder, position: Int) {
        val todoItem = items[position]
        holder.bind(todoItem, onItemCheckChange, onEditClick, onDeleteClick)
    }

    override fun getItemCount(): Int = items.size

    fun updateItems(newItems: List<ToDoItem>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    class ToDoItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val checkBox: CheckBox = itemView.findViewById(R.id.checkbox_todo_item)
        private val editButton: ImageButton = itemView.findViewById(R.id.edit_button)
        private val deleteButton: ImageButton = itemView.findViewById(R.id.delete_button)

        private val context: Context = itemView.context
        private val sharedPreferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

        fun bind(
            todoItem: ToDoItem,
            onItemCheckChange: (ToDoItem, Boolean) -> Unit,
            onEditClick: (ToDoItem) -> Unit,
            onDeleteClick: (ToDoItem) -> Unit
        ) {
            checkBox.text = todoItem.name
            checkBox.isChecked = todoItem.isCompleted

            val completedColor = sharedPreferences.getInt("completed_color", R.color.green)
            val notCompletedColor = sharedPreferences.getInt("not_completed_color", R.color.brown)

            val color = if (todoItem.isCompleted) completedColor else notCompletedColor
            checkBox.setTextColor(ContextCompat.getColor(context, color))

            checkBox.setOnCheckedChangeListener { _, isChecked ->
                onItemCheckChange(todoItem, isChecked)
            }
            editButton.setOnClickListener {
                onEditClick(todoItem)
            }
            deleteButton.setOnClickListener {
                AlertDialog.Builder(context)
                    .setTitle("Are you sure?")
                    .setMessage("Do you want to delete this item?")
                    .setNegativeButton("CANCEL", null)
                    .setPositiveButton("CONTINUE") { _, _ ->
                        onDeleteClick(todoItem)
                    }
                    .create()
                    .show()
            }
        }
    }
}
