package com.example.todolist

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase

class ToDoRepository(context: Context) {

    private val dbHelper = ToDoDatabaseHelper(context)
    private val db: SQLiteDatabase = dbHelper.writableDatabase

    fun addList(name: String): Long {
        val values = ContentValues().apply {
            put(ToDoDatabaseHelper.COLUMN_LIST_NAME, name)
        }
        return db.insert(ToDoDatabaseHelper.TABLE_LISTS, null, values)
    }

    fun getLists(): List<ToDoList> {
        val lists = mutableListOf<ToDoList>()
        val cursor: Cursor = db.query(ToDoDatabaseHelper.TABLE_LISTS, null, null, null, null, null, null)
        with(cursor) {
            while (moveToNext()) {
                val id = getLong(getColumnIndexOrThrow(ToDoDatabaseHelper.COLUMN_LIST_ID))
                val name = getString(getColumnIndexOrThrow(ToDoDatabaseHelper.COLUMN_LIST_NAME))
                val items = getItems(id)
                lists.add(ToDoList(id, name, items))
            }
        }
        cursor.close()
        return lists
    }

    fun addItem(listId: Long, name: String, isCompleted: Boolean): Long {
        val values = ContentValues().apply {
            put(ToDoDatabaseHelper.COLUMN_ITEM_NAME, name)
            put(ToDoDatabaseHelper.COLUMN_ITEM_COMPLETED, if (isCompleted) 1 else 0)
            put(ToDoDatabaseHelper.COLUMN_ITEM_LIST_ID, listId)
        }
        return db.insert(ToDoDatabaseHelper.TABLE_ITEMS, null, values)
    }

    fun getItems(listId: Long): List<ToDoItem> {
        val items = mutableListOf<ToDoItem>()
        val selection = "${ToDoDatabaseHelper.COLUMN_ITEM_LIST_ID} = ?"
        val selectionArgs = arrayOf(listId.toString())
        val cursor: Cursor = db.query(ToDoDatabaseHelper.TABLE_ITEMS, null, selection, selectionArgs, null, null, null)
        with(cursor) {
            while (moveToNext()) {
                val id = getLong(getColumnIndexOrThrow(ToDoDatabaseHelper.COLUMN_ITEM_ID))
                val name = getString(getColumnIndexOrThrow(ToDoDatabaseHelper.COLUMN_ITEM_NAME))
                val isCompleted = getInt(getColumnIndexOrThrow(ToDoDatabaseHelper.COLUMN_ITEM_COMPLETED)) == 1
                items.add(ToDoItem(id, name, isCompleted, listId))
            }
        }
        cursor.close()
        return items
    }

    fun updateItem(item: ToDoItem) {
        val values = ContentValues().apply {
            put(ToDoDatabaseHelper.COLUMN_ITEM_NAME, item.name)
            put(ToDoDatabaseHelper.COLUMN_ITEM_COMPLETED, if (item.isCompleted) 1 else 0)
        }
        val selection = "${ToDoDatabaseHelper.COLUMN_ITEM_ID} = ?"
        val selectionArgs = arrayOf(item.id.toString())
        db.update(ToDoDatabaseHelper.TABLE_ITEMS, values, selection, selectionArgs)
    }

    fun deleteItem(itemId: Long) {
        val selection = "${ToDoDatabaseHelper.COLUMN_ITEM_ID} = ?"
        val selectionArgs = arrayOf(itemId.toString())
        db.delete(ToDoDatabaseHelper.TABLE_ITEMS, selection, selectionArgs)
    }

    fun updateList(list: ToDoList) {
        val values = ContentValues().apply {
            put(ToDoDatabaseHelper.COLUMN_LIST_NAME, list.name)
        }
        val selection = "${ToDoDatabaseHelper.COLUMN_LIST_ID} = ?"
        val selectionArgs = arrayOf(list.id.toString())
        db.update(ToDoDatabaseHelper.TABLE_LISTS, values, selection, selectionArgs)
    }

    fun deleteList(listId: Long) {
        val selection = "${ToDoDatabaseHelper.COLUMN_LIST_ID} = ?"
        val selectionArgs = arrayOf(listId.toString())
        db.delete(ToDoDatabaseHelper.TABLE_ITEMS, "${ToDoDatabaseHelper.COLUMN_ITEM_LIST_ID} = ?", selectionArgs)
        db.delete(ToDoDatabaseHelper.TABLE_LISTS, selection, selectionArgs)
    }
}
